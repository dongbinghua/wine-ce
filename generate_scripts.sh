#!/bin/sh
script_txt=" #!/bin/sh                                                                          \n
script_path=\$0                                                                                 \n
if [ -h \$script_path ]                                                                         \n
then                                                                                            \n
    script_path=\$(readlink \$script_path)                                                      \n
fi                                                                                              \n
if [ \$# -lt 1 ] || [ \"\$1\" = \"--help\" ]  ; then                                            \n
    echo \"Usage: \$script_path PROGRAM [ARGUMENTS...]   Run the specified program\"            \n
    echo \"       \$script_path --help                   Display this help and exit\"           \n
    echo \"       \$script_path --version                Output version information and exit\"  \n
    exit 1;                                                                                     \n
fi                                                                                              \n
if [ \$# -lt 1 ] || [ \"\$1\" = \"--version\" ]  ; then                                         \n
    echo \`\$(dirname \$script_path)/wine --version\`                                           \n
    exit 1;                                                                                     \n
fi                                                                                              \n
current_path=\`pwd\`                                                                            \n
wine_dir=\$(cd \$(dirname \$(dirname \$script_path));pwd)                                       \n
wine_unix_path=\$(cd \$wine_dir/lib/wine/*-unix;pwd)                                            \n
host_arch=\`echo \$(basename \$wine_unix_path) | cut -d - -f 1\`                                \n
guest_arch=\`echo \$(basename \$script_path) | cut -d - -f 2\`                                  \n
wine_pe_path=\$wine_dir/lib/wine/\$guest_arch-windows                                           \n
cd \$current_path                                                                               \n                                                                                         \n
export WINENTDLLPATH=\$wine_unix_path/ntdll.so                                                  \n
export WINEDLLPATH=\$wine_dir/lib/wine                                                          \n
exec \$wine_dir/bin/qemu-\$guest_arch \$wine_pe_path/qemuloader \$@ "

install_script_txt=" #!/bin/sh                                      \n
script_path=\$0                                                     \n
if [ -h \$script_path ]                                             \n
then                                                                \n
    script_path=\$(readlink \$script_path)                          \n
fi                                                                  \n
script_dir=\$(cd \$(dirname \$script_path);pwd)                     \n
guest_arch=\"i386\"                                                 \n
if [ -e \$script_dir/wine-\$guest_arch ] ; then                     \n
    chmod +x \$script_dir/wine-\$guest_arch                         \n
    ln -sf \$script_dir/wine-\$guest_arch /usr/bin/wine-\$guest_arch\n
fi                                                                  \n
guest_arch=\"x86_64\"                                               \n
if [ -e \$script_dir/wine-\$guest_arch ] ; then                     \n
    chmod +x \$script_dir/wine-\$guest_arch                         \n
    ln -sf \$script_dir/wine-\$guest_arch /usr/bin/wine-\$guest_arch\n
fi                                                                  \n
guest_arch=\"arm\"                                                  \n
if [ -e \$script_dir/wine-\$guest_arch ] ; then                     \n
    chmod +x \$script_dir/wine-\$guest_arch                         \n
    ln -sf \$script_dir/wine-\$guest_arch /usr/bin/wine-\$guest_arch\n
fi                                                                  \n
guest_arch=\"aarch64\"                                              \n
if [ -e \$script_dir/wine-\$guest_arch ] ; then                     \n
    chmod +x \$script_dir/wine-\$guest_arch                         \n
    ln -sf \$script_dir/wine-\$guest_arch /usr/bin/wine-\$guest_arch\n
fi                                                                  \n
if [ -e \$script_dir/wine ] ; then                                  \n
    chmod +x \$script_dir/wine                                      \n
    ln -sf \$script_dir/wine /usr/bin/wine                          \n
fi                                                                  \n"

script_path=$0
if [ -h $script_path ]
then
    script_path=$(readlink $script_path)
fi

if [ $# -lt 1 ] || [ "$1" = "--help" ] || [ ! -d $1 ] || [ ! -e $1/wine ]; then
    echo "Usage: $script_path wine_install_dir/bin"
    exit 1;
fi

script_dir=$(cd $(dirname $script_path);pwd)
install_dir=$(cd $1;pwd)

echo $install_script_txt>$install_dir/install.sh

guest_arch="i386"
if [ -e $install_dir/qemu-i386 ] ; then
    echo $script_txt>$install_dir/wine-$guest_arch
    chmod +x $install_dir/wine-$guest_arch
fi

guest_arch="x86_64"
if [ -e $install_dir/qemu-$guest_arch ] ; then
    echo $script_txt>$install_dir/wine-$guest_arch
    chmod +x $install_dir/wine-$guest_arch
fi

guest_arch="arm"
if [ -e $install_dir/qemu-$guest_arch ] ; then
    echo $script_txt>$install_dir/wine-$guest_arch
    chmod +x $install_dir/wine-$guest_arch
fi

guest_arch="aarch64"
if [ -e $install_dir/qemu-$guest_arch ] ; then
    echo $script_txt>$install_dir/wine-$guest_arch
    chmod +x $install_dir/wine-$guest_arch
fi